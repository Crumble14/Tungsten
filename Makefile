NAME = Tungsten
CC = gcc
CFLAGS = -Wall -Wextra -Werror -std=c++17 -pthread -rdynamic

DEBUG = true

ifeq ($(DEBUG), true)
	CFLAGS += -O0 -g3 -D TUNGSTEN_VM_DEBUG
else
	CFLAGS += -O3 -g0
endif

SRC_DIR = src/
SRC := $(shell find $(SRC_DIR) -type f -name "*.cpp")
HDR := $(shell find $(SRC_DIR) -type f -name "*.hpp")

DIRS := $(shell find $(SRC_DIR) -type d)

OBJ_DIR = obj/
OBJ_DIRS := $(patsubst $(SRC_DIR)%, $(OBJ_DIR)%, $(DIRS))
OBJ := $(patsubst $(SRC_DIR)%.cpp, $(OBJ_DIR)%.o, $(SRC))

LIBS = -lstdc++ -lm -ldl

CORES := $(shell nproc)

all: $(NAME)

$(NAME): mkdir $(OBJ)
	echo "Linking..."
	$(CC) $(CFLAGS) -o $(NAME) $(OBJ) $(LIBS)

lib: mkdir $(OBJ)
	echo "Creating archive..."
	ar rc $(NAME).a $(OBJ)

mkdir:
	echo "Making object directories..."
	mkdir -p $(OBJ_DIRS)

$(OBJ_DIR)%.o: $(SRC_DIR)%.cpp $(HDR)
	@echo "Compiling:" $<
	@$(CC) $(CFLAGS) -c $< -o $@ $(LIBS)

tags:
	ctags -R src/* --languages=c,c++

clean:
	echo "Cleaning objects..."
	rm -rf $(OBJ_DIR)

fclean: clean
	echo "Removing binaries..."
	rm -f $(NAME)
	rm -f $(NAME).a

re: fclean all

.PHONY: all $(NAME) lib mkdir tags clean fclean re
.SILENT: all $(NAME) lib mkdir tags clean fclean re
