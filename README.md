<p align="center">
<img src="https://gitlab.com/Crumble14/Tungsten/raw/master/logo.png" />
</center>

Tungsten
--------

Remotely Tungsten is an open-source virtual machine working with Tungsten bytecode.



# Compilation

To compile Tungsten, you need:
- Git
- Make
- G++

First, clone the **Tungsten** and **Tungsten\_std** repositories:

```
git clone https://gitlab.com/Crumble14/Tungsten
git clone https://gitlab.com/Crumble14/Tungsten_std
```

Then, go inside the Tungsten folder and build the project:

```
cd Tungsten
make -j
```

Do the same for the standard library:

```
cd ../Tungsten_std
make -j
```

Finally, you can install the standard library:

```
cp libTungsten_std.so ../Tungsten/std.so
mkdir ../Tungsten/std
find src/ -type f ! -name "*pp" | xargs cp -t ../Tungsten/std
cd ../Tungsten
```



# Virtual Machine

The virtual machine allows to run compiled Tungsten programs.
It can be used with the command: ``tungsten -r <File> [Flags]``



## Program bytecode

The bytecode of a program contains all the instructions of that program in binary format.
This format allows to read and execute th program faster.

The bytecode is made of two parts:
- **The header** which contains metadata
- **The body** which contains the program's instructions



### Header

The header begins with a **32 bits long integer** which defines its size.
It contains constants that will be used by the program.

Constant types are:

| #    | Name   | Description                    |
|------|--------|--------------------------------|
| 0x00 | native | The name of a native function. |
| 0x01 | string | A characters sequence.         |



All the constants are stored one after the other in the following format:
- **Type** (1 byte): The constant type
- **Value**: The constant's value
- **End** (1 byte): The ``\0`` character to terminate the value



### Body

The body contains all the instructions of the program, one after the other.

The structure of an instruction is the following:
- **Opcode** (8 bits): The operation code
- **Valsize** (8 bits): The size of the value(s) the instruction operates on
- **Args**: (4 * 8 bits): The arguments for the instruction

Thus, the total size is **48 bits**.



## Opcodes

Opcode types:

- **0x00 .. 0x03**: Stack
- **0x04 .. 0x0d**: Arithmetic
- **0x0e .. 0x13**: Logical
- **0x14 .. 0x19**: Comparison
- **0x1a .. 0x1d**: Jump
- **0x1e .. 0x23**: Memory
- **0x24 .. 0x25**: Other

Opcodes list:

| #    | Name    | Arguments   | Description                                                                              |
|------|---------|-------------|------------------------------------------------------------------------------------------|
| 0x00 | push    | value       | Pushes the value in argument onto the stack.                                             |
| 0x01 | cpush   | id          | Pushes a pointer to the specified constant onto the stack.                               |
| 0x02 | pop     |             | Pops the value on top of the stack.                                                      |
| 0x03 | dup     |             | Duplicates the value on top of the stack.                                                |
| 0x04 | add     |             | Performs an addition. \*                                                                 |
| 0x05 | sub     |             | Performs a subtraction. \*                                                               |
| 0x06 | mul     |             | Performs a multiplication. \*                                                            |
| 0x07 | div     |             | Performs a division. \*                                                                  |
| 0x08 | mod     |             | Performs a modulo. \*                                                                    |
| 0x09 | pow     |             | Performs a power. \*                                                                     |
| 0x0a | incr    |             | Increments the value on top of the stack.                                                |
| 0x0b | decr    |             | Decrements the value on top of the stack.                                                |
| 0x0c | neg     |             | Negates the value on top of the stack.                                                   |
| 0x0d | inv     |             | Inverts the value on top of the stac.                                                    |
| 0x0e | not     |             | Performs a bitwise NOT.                                                                  |
| 0x0f | and     |             | Performs a bitwise AND. \*                                                               |
| 0x10 | or      |             | Performs a bitwise OR. \*                                                                |
| 0x11 | xor     |             | Performs a bitwise WOR. \*                                                               |
| 0x12 | left    |             | Performs a bitwise LEFT SHIFT. \*                                                        |
| 0x13 | right   |             | Performs a bitwise RIGHT SHIFT. \*                                                       |
| 0x14 | eq      |             | Compares values (==) and pushes the result. \*                                           |
| 0x15 | neq     |             | Compares values (!=) and pushes the result. \*                                           |
| 0x16 | low     |             | Compares values (<) and pushes the result. \*                                            |
| 0x17 | lowe    |             | Compares values (<=) and pushes the result. \*                                           |
| 0x18 | high    |             | Compares values (>) and pushes the result. \*                                            |
| 0x19 | highe   |             | Compares values (>=) and pushes the result. \*                                           |
| 0x1a | jmp     | instruction | Jumps to the specified instruction.                                                      |
| 0x1b | ifjmp   | instruction | Jumps to the specified instruction if the value on top of the stack doesn't equal 0.     |
| 0x1c | call    | instruction | Calls the function beginning at the specified instruction.                               |
| 0x1d | ret     |             | Returns the current function.                                                            |
| 0x1e | vcreate |             | Creates a local variable with the specified index.                                       |
| 0x1f | vstore  | position    | Stores the value on top of the stack into the specified local variable and pops it.      |
| 0x20 | vload   | position    | Loads the value from the specified local variable and pushes it.                         |
| 0x21 | vptr    | position    | Pushes the pointer of the specified local variable.                                      |
| 0x22 | mstore  |             | Stores the value on top of the stack at the pointer at the second position of the stack. |
| 0x23 | mload   |             | Loads the value at the pointer on top of the stack, pushes it and pops the pointer.      |
| 0x24 | ncall   | native      | Calls the specified native function.                                                     |
| 0x25 | exit    |             | Stops the program.                                                                       |

\*: The instruction takes the 2 values on top of the stack, pops them and pushes the result.



## Memory management

There are two ways of allocating memory:
- **Static allocation**: The memory is allocated in a function and removed when the function returns
- **Dynamic allocation**: The memory is manually allocated and freed (can potentially make memory leaks)



### Static allocation

**The stack** allows to manage memory through the principe of **LIFO** (last in, first out).
Thus, only the value on top of the stack can be accessed.

**Local variables** are just like global variables but are removed when the current function is returned.



### Dynamic allocation

Dynamic allocation allows to manually manage memory through native calls and pointers direct access.
This way of management allows, for example, to create arrays or objects, but can also cause memory leaks in case of bad usage.



# Assembler

The assembler allows to convert Tungten assembly programs (.wa files) into compiled Tungsten compiled programs (.wp files).
It can be used through the command: ``tungsten -a <File>``

The output file is named **out.wp**.



# Tungsten language

The Tungsten language is a high-level language running on the TVM.



## Keywords

Here is the list of all keywords:

| Keyword   | Description                                        |
|-----------|----------------------------------------------------|
| void      | Empty or an unknown data type                      |
| int8      | Data type                                          |
| uint8     | Data type                                          |
| int16     | Data type                                          |
| uint16    | Data type                                          |
| int32     | Data type                                          |
| uint32    | Data type                                          |
| int64     | Data type                                          |
| uint64    | Data type                                          |
| float     | Data type                                          |
| double    | Data type                                          |
| bool      | Data type                                          |
| false     | Boolean value                                      |
| true      | Boolean value                                      |
| null      | A null pointer                                     |
| const     | Declares a constant                                |
| constexpr | Declares a constant expression                     |
| namespace | Declares a namespace                               |
| private   | Access specifier                                   |
| protected | Access specifier                                   |
| public    | Access specifier                                   |
| inline    | Declares a function inline                         |
| return    | Returns a function                                 |
| if        | Declares an if statement                           |
| else      | Declares an else statement                         |
| switch    | Declares a switch statement                        |
| case      | Declares a case statement                          |
| default   | Declares a default statement                       |
| while     | Declares a while statement                         |
| for       | Declares a for statement                           |
| do        | Declares a do statement                            |
| continue  | Skips the remaining content of a loop              |
| break     | Terminates a loop or a switch statement            |
| try       | Declares a try statement                           |
| catch     | Declares a catch statement                         |
| throw     | Throws an exception                                |
| struct    | Declares a structure                               |
| this      | Designates the current structure                   |
| super     | Designates the parent structure                    |
| operator  | Declares an operator overloading                   |
| new       | Structure dynamic allocation                       |
| delete    | Structure dynamic deallocation                     |
| template  | Declares a template                                |
| sizeof    | Returns the size of the specified type or variable |
| asm       | Declares an assembly block                         |



## Variables

```
["const"] type["*"] name ["=" value]
```

A variable allows to allocate memory to store data and to put a name on it.

Variables can only be declared inside of functions and structures unless it's declared ``const``.
The name of a variable has to be unique in the current scope.

Variable types are the following:

| Name   | Type                       | Size (bytes) | Value range                               |
|--------|----------------------------|--------------|-------------------------------------------|
| int8   | integer/character          | 1            | -128 to 127                               |
| uint8  | unsigned integer/character | 1            | 0 to 255                                  |
| int16  | integer                    | 2            | -32768 to 32767                           |
| uint16 | unsigned integer           | 2            | 0 to 65535                                |
| int32  | integer                    | 4            | -2147483648 to 2147483647                 |
| uint32 | unsigned integer           | 4            | 0 to 4294967295                           |
| int64  | integer                    | 8            | -9,223372037\*10^18 to 9,223372037\*10^18 |
| uint64 | unsigned integer           | 8            | 0 to 1,844674407\*10^19                   |
| float  | floating point integer     | 4            |                                           |
| double | floating point integer     | 8            |                                           |
| bool   | boolean                    | 1            | false or true                             |

If a ``*`` is placed after the type, then the type is a pointer to this type.

A variable can be defined with the following declaration:

```
name = value
```

A variable cannot be redefined if it's declared ``const``.
If the value is too large or too small to fit, it's implicitly cast.



### Pointers

Pointers are **8 bits long unsigned integer** values representing an address on the memory.

The pointer of a variable can be retrieved with the following declaration:

```
&variable
```

The value stored at a pointer can be retrieved with the following declaration:

```
*variable
```



## Operators

Operators are the following:

| Operator | Overloadable | Description                                             |
|----------|--------------|---------------------------------------------------------|
| +        | true         | Adds two values                                         |
| -        | true         | Subtracts two values                                    |
| \*       | true         | Multiplies two values                                   |
| /        | true         | Divides two values                                      |
| %        | true         | Performs a modulo on two values                         |
| \*\*     | true         | Raises the first value to the power of the second value |
| ++       | true         | Increments a variable                                   |
| --       | true         | Decrements a variable                                   |
| ~        | true         | Bitwise NOT                                             |
| &        | true         | Bitwise AND                                             |
| \|       | true         | Bitwise OR                                              |
| ^        | true         | Bitwise XOR                                             |
| <<       | true         | Bitwise LEFT SHIFT                                      |
| >>       | true         | Bitwise RIGHT SHIFT                                     |
| =        | true         | Assigns a value to a variable                           |
| +=       | true         | Add assign                                              |
| -=       | true         | Subtract assign                                         |
| \*=      | true         | Multiply assign                                         |
| /=       | true         | Divide assign                                           |
| %=       | true         | Modulo assign                                           |
| \*\*=    | true         | Power assign                                            |
| &        | false        | Address-of                                              |
| *        | false        | Indirection                                             |
| ==       | true         | Compares two values                                     |
| !=       | true         | Compares two values                                     |
| <        | true         | Compares two values                                     |
| <=       | true         | Compares two values                                     |
| >        | true         | Compares two values                                     |
| >=       | true         | Compares two values                                     |
| ?        | false        | Ternary condition                                       |
| :        | false        | Ternary else                                            |
| !        | true         | Inverse value                                           |
| ()       | true         | Function call                                           |
| []       | true         | Array access                                            |
| .        | false        | Member access                                           |



## Namespaces

```
"namespace" name
"{"
	...
"}"
```

Namespaces allow to separate the code into sections to manage functions permissions, thanks to **access specifiers**.
A namespace can be declared several times. Every definition is added to the others.



## Access specifiers

Access specifiers can be placed in front of global constants, functions and structures.

Here are all of them:

| Name      | In global scope      | In namespace                                 | In structure                               |
|-----------|----------------------|----------------------------------------------|--------------------------------------------|
| public    | Everywhere (default) | Everywhere                                   | Everywhere                                 |
| protected | Everywhere           | From current and parent namespaces (default) | From current structure and child structure |
| private   | Everywhere           | From current namespace only                  | From current structure only (default)      |



## Functions

```
["inline"] return name"("#(["const"] type["*"]["&"] name)") const"
"{"
	...
"}"
```

Functions are block of code that can be called to perform the instructions in it.
A function can be placed anywhere, except inside another function.

If a function is defined inside a structure, it becomes a member of that structure.

A non-void function must return a value anyway.
In the same way, a void function can't return any value.

Parameters are treated like regular local values.
If a parameter has ``&`` after it's type, them it's a reference, it means that the value isn't duplicated at function call.

```
"return"["("value")"]
```

Return statements allows to return the current function and can take a value.

```
function"("#(parameter)")"
```

The above delaration is a function call.





## Conditions

Conditions allow to make operation being performed only if a specified condition is fullfilled.



### If statements

```
"if("value")"
"{"
	...
"}"
*("else if("value")"
"{"
	...
"}")
["else"
"{"
	...
"}"]
```

The instructions inside of an ``if`` block are executed if the value passed in it doesn't equal ``0``.
An ``else`` statement can be added to perform an operations if the condition is not fullfilled.
``else``-``if`` statements can also be added to check for several values and perform operations according to the first one which is fullfilled.



### Switch statements

TODO



## Loops

Loops allow to perform operations while a condition is fullfilled.



### While loops

```
"while("value")"
"{"
	...
"}"
```

A ``while`` loop repeats the instructions in its scope until the passed value equals 0.

```
break
```

The break statement allows to get out of the while even if the value doesn't equal 0, skipping the remaining instructions.

```
continue
```

The continue statement jumps back to the loop beginning and enters in it if the value doesn't equal 0.



### For loops

TODO



### Do..while loops

```
"do"
"{"
	...
"}"
"while("value")"
```

``do``..``while`` loops act just like ``while`` loops, but the condition is checked at the end.
It means that the instructions in the scope will be executed at least one time.



## Exceptions

TODO



## Structures

```
"struct" name "^" #superstruct
"{"
	...
"}"
```

Structures allow to declare new types that can contain several variables and functions. Structures can be declared anywhere, except into functions and structures.
The size of this variable is the summation of the size of all its fields.

```
["const"] type name["("#parameter")"]
```

If a function operator is placed at the end of a structure instanciation, then the constructor of this structure is called with the parameters.
If the structure has a constructor that takes parameters and no constructor that takes no parameter, then passing parameters during instanciation becomes mandatory.

A member of a structure can be accessed using the ``.`` operator.



### Constructor

```
"__("#parameter")"
"{"
	...
"}"
```

A constructor is a function called at structure instanciation.
It cannot return any value.



### Destructor

```
"~~()"
"{"
	...
"}"
```

A destructor is a function called before structure deallocation.
It cannot take parameters or return any value.



## Templates

TODO



## Sizeof

```
"sizeof("(value | type)")"
```

The ``sizeof`` statement returns the size of a value or a type in octets.



## Assembly blocks

```
"asm"
"{"
	...
"}"
```

``asm`` blocks allow to write assembly instructions inside the program.
Assembly instructions have to be place directly into the block.
