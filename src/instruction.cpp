#include "tungsten.hpp"

using namespace Tungsten;

const char* opcodes[] = {
	"push",
	"cpush",
	"pop",
	"dup",

	"add",
	"sub",
	"mul",
	"div",
	"mod",
	"pow",
	"incr",
	"decr",
	"neg",
	"inv",

	"not",
	"and",
	"or",
	"xor",
	"left",
	"right",

	"eq",
	"neq",
	"low",
	"lowe",
	"high",
	"highe",

	"jmp",
	"ifjmp",
	"call",
	"ret",

	"vcreate",
	"vstore",
	"vload",
	"vptr",
	"mstore",
	"mload",

	"ncall",
	"exit"
};

const char* Tungsten::get_opcode(const Opcode opcode)
{
	return opcodes[opcode];
}

Opcode Tungsten::get_opcode(const char* str)
{
	for(uint8_t o = 0; o < sizeof(opcodes) / sizeof(const char*); ++o) {
		if(strcmp(opcodes[o], str) == 0) return (Opcode) o;
	}

	throw invalid_argument("Unknown opcode!");
}

string Instruction::dump() const
{
	string str(get_opcode(opcode));
	str += to_string(size);

	for(size_t i = 0; i < sizeof(args) / sizeof(uint8_t); ++i) {
		str += ' ';
		str += get_hexa(get_arg<uint8_t>(i));
	}

	return str;
}
