#include "tungsten.hpp"

using namespace Tungsten;

void Program::read(const string& file)
{
	ifstream stream(file);
	stream.seekg(0, ios::end);

	const uint32_t size = stream.tellg();
	if(size < sizeof(uint32_t)) throw bytecode_exception();

	stream.seekg(0);

	uint32_t header_size;
	stream.read((char*) &header_size, sizeof(header_size));
	if(header_size > size) throw bytecode_exception();

	if(header_size > 0) {
		stream.seekg(sizeof(header_size));

		char* header;
		if(!(header = (char*) malloc(header_size))) throw bad_alloc();
		stream.read(header, header_size);

		constants.clear();
		size_t i = 0;

		while(i < header_size) {
			const ConstantType type = (ConstantType) header[i];
			++i;

			string value;

			while(header[i]) {
				value += header[i];
				++i;
			}

			++i;
			constants.emplace_back(type, value);
		}

		free(header);
	}

	if((size - header_size - sizeof(header_size)) % sizeof(Instruction) != 0) {
		throw bytecode_exception();
	}

	instructions.clear();
	instructions.resize((size - header_size) / sizeof(Instruction));

	stream.seekg(sizeof(header_size) + header_size);
	stream.read((char*) instructions.data(), size - header_size);
}

void Program::write(const string& file) const
{
	ofstream stream(file, ios::trunc);
	uint32_t header_size = 0;

	stream.seekp(sizeof(header_size));

	for(size_t i = 0; i < constants.size(); ++i) {
		const auto& c = constants[i];
		const auto size = c.value.size() + 1;

		stream.seekp(sizeof(header_size) + header_size);
		stream.write((const char*) &c.type, sizeof(c.type));
		header_size += sizeof(c.type);

		stream.seekp(sizeof(header_size) + header_size);
		stream.write(c.value.c_str(), size);
		header_size += size;
	}

	stream.seekp(0);
	stream.write((const char*) &header_size, sizeof(header_size));

	for(size_t i = 0; i < instructions.size(); ++i) {
		const auto& inst = instructions[i];

		stream.seekp(sizeof(header_size) + header_size + (i * sizeof(inst)));
		stream.write((const char*) &inst, sizeof(inst));
	}
}
