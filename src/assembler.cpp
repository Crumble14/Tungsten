#include "tungsten.hpp"
#include "compiler/compiler.hpp"

using namespace Tungsten;

static void trim(string& str)
{
	while(!str.empty()) {
		if(!is_space(str.back())) break;
		str.pop_back();
	}

	while(!str.empty()) {
		if(!is_space(str.front())) break;
		str.erase(0, 1);
	}
}

static void foreach_line(const string& str,
	const function<void(const size_t, const string&)> handle)
{
	stringstream ss(str);
	string buffer;

	size_t line = 0;

	while(getline(ss, buffer)) {
		++line;

		// TODO Add comment removal
		trim(buffer);
		if(buffer.empty()) continue;

		handle(line, buffer);
	}
}

static bool is_valid_function_name(const char* str)
{
	while(*str) {
		if(is_space(*str) || *str == '-' || *str == ':') {
			return false;
		}

		++str;
	}

	return true;
}

string get_opcode_word(const char*& str)
{
	string opcode;

	while(*str && !is_space(*str) && !is_numeric(*str)) {
		opcode += *str;
		++str;
	}

	return opcode;
}

static uint8_t get_size(const char*& str)
{
	string s;

	while(*str && is_numeric(*str))
	{
		s += *str;
		++str;
	}

	if(*str && !is_space(*str)) {
		throw invalid_argument("Parse error!");
	}

	if(s.empty()) return 4;

	const unsigned int i = stoi(s);
	if(i > 255) throw invalid_argument("Too large size!");
	return i;
}

static int arg_atoi(const char*& str)
{
	while(*str <= 32 || *str == 127) ++str;

	int n = 0;
	int8_t neg = (*str == '-' ? -1 : 1);
	if(*str == '+' || *str == '-') ++str;

	while(is_numeric(*str)) {
		n *= 10;
		n += *str - '0';

		++str;
	}

	return neg * n;
}

static uint32_t get_args(const char*& str)
{
	uint32_t args = 0;

	while(*str) {
		args <<= 8;

		if(*str == '"') {
			throw invalid_argument("String allowed only for push!");
		} else if(*str == '\'') {
			++str;

			const bool backslash = (*str == '\\');
			if(backslash) ++str;

			args |= (backslash ? translate_escape_code(*str) : *str);
			++str;

			if(*str != '\'') {
				throw invalid_argument("Invalid character declaration!");
			}

			++str;
		} else if(is_numeric(*str)) {
			const auto i = arg_atoi(str);

			if(i < 255) {
				args |= (uint8_t) i;
			} else {
				args = i;
			}
		} else {
			throw invalid_argument("Invalid argument!");
		}

		skip_spaces(str);
	}

	return args;
}

Program Assembler::assemble()
{
	handle_functions();
	handle_instructions();

	return program;
}

void Assembler::handle_functions()
{
	uint32_t i = 0;

	foreach_line(assembly, [this, &i](const size_t l,
		const string& line)
		{
			try {
				if(line.back() == ':') {
					const string name(line.cbegin(), line.cend() - 1);

					if(!is_valid_function_name(name.c_str())) {
						throw invalid_argument("Invalid function name!");
					}

					functions.emplace(name, i);
				} else {
					++i;
				}
			} catch(const invalid_argument& e) {
				throw invalid_argument(string(e.what())
					+ " (Line: " + to_string(l) + ")");
			}
		});
}

void Assembler::handle_instructions()
{
	foreach_line(assembly, [this](const size_t l,
		const string& line)
		{
			try {
				if(line.back() == ':') return;

				const char* str = line.c_str();

				const auto opcode = get_opcode(get_opcode_word(str).c_str());
				const auto size = get_size(str);

				skip_spaces(str);

				if(opcode == CPUSH) {
					// TODO Handle other values than just strings?
					handle_string_push(str);
				} else if(opcode == CALL) {
					const string function(str);
					const auto i = get_function_begin(function);

					program.instructions.emplace_back(opcode, size, i);
				} else if(opcode == NCALL) {
					const string native(str);

					if(!is_valid_function_name(native.c_str())) {
						throw invalid_argument("Invalid native name: `"s
							+ native + "`!");
					}

					const auto i = get_constant_id(NATIVE_C, native);

					program.instructions.emplace_back(opcode, size, i);
				} else {
					program.instructions.emplace_back(opcode, size, get_args(str));
				}
			} catch(const invalid_argument& e) {
				throw invalid_argument(string(e.what())
					+ " (Line: " + to_string(l) + ")");
			}
		});
}

uint32_t Assembler::get_function_begin(const string& name)
{
	const auto f = functions.find(name);
	if(f == functions.cend()) throw invalid_argument("Function `"s
		+ name + "` not found!");

	return f->second;
}

uint32_t Assembler::get_constant_id(const ConstantType type,
	const string& value)
{
	for(size_t i = 0; i < program.constants.size(); ++i) {
		const auto& c = program.constants[i];

		if(c.type == type && c.value == value) {
			return i;
		}
	}

	program.constants.emplace_back(type, value);
	return program.constants.size() - 1;
}

void Assembler::handle_string_push(const char*& str)
{
	if(*str != '"') {
		throw invalid_argument("Invalid value for cpush!");
	}

	++str;

	string value;
	bool backslash = false;

	while(*str) {
		if(*str == '\\' && !backslash) {
			backslash = true;
		} else if(*str == '"' && !backslash) {
			++str;
			break;
		} else {
			value += (backslash ? translate_escape_code(*str) : *str);
			backslash = false;
		}

		++str;
	}

	if(*str) {
		throw invalid_argument("Instruction cpush takes only one argument!");
	}

	program.instructions.emplace_back(CPUSH, 8, get_constant_id(STRING_C, value));
}
