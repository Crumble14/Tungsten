#include "tungsten.hpp"
#include "compiler/compiler.hpp"
#include "compiler/parser/parser.hpp"

using namespace Tungsten;

static void show_help()
{
	cout << "Remotely Tungsten version " << VERSION << '\n';
	cout << "Website: http://tungsten.remotely.fr" << '\n';
	cout << '\n';
	cout << "--- Help ---" << '\n';
	cout << "Syntax: tungsten <option> [file] [flags]" << '\n';
	cout << "Options:" << '\n';
	cout << "\t-h\tShows help" << '\n';
	cout << "\t-pc\tPrecompiles the specified file and prints the result out on the standard output" << '\n';
	cout << "\t-st\tPrints out the syntax tree of the specified source file on the standard output" << '\n';
	cout << "\t-ca\tCompiles to assembly" << '\n';
	cout << "\t-cb\tCompiles to bytecode" << '\n';
	cout << "\t-a\tAssembles the specified file" << '\n';
	cout << "\t-r\tRuns the specified program" << '\n';
	cout << '\n';
	cout << "VM debug flags (only work on debug build of the VM):" << '\n';
	cout << "\td\tShows debug messages" << '\n';
	cout << "\ti\tShows the instructions that are being executed" << '\n';
}

static inline void print_tabs(const size_t tabs)
{
	for(size_t i = 0; i < tabs; ++i) cout << '\t';
}

static void print_ast(const AST* ast, const size_t level)
{
	print_tabs(level);
	cout << ast->to_string() << '\n';

	if(!ast->get_children().empty()) {
		print_tabs(level + 1);
		cout << "\033[33mChildren:\033[0m" << '\n';

		for(const auto& s : ast->get_children()) {
			print_ast(s.get(), level + 2);
		}
	}

	if(!ast->get_scope().empty()) {
		print_tabs(level + 1);
		cout << "\033[36mScope:\033[0m" << '\n';

		for(const auto& s : ast->get_scope()) {
			print_ast(s.get(), level + 2);
		}
	}
}

static void precompile_program(const string& file)
{
	try {
		cout << precompile(file);
	} catch(const exception& e) {
		cerr << e.what() << '\n';
	}
}

static void print_syntax_tree(const string& file)
{
	try {
		const auto ast = parse(precompile(file));

		cout << "Syntax tree for file `" << file << "`:" << '\n';
		print_ast(ast.get(), 0);
	} catch(const exception& e) {
		cerr << e.what() << '\n';
	}
}

static void compile_to_assembly(const string& file)
{
	try {
		cout << "\033[92mPrecompiling...\033[0m" << '\n';

		const auto src = precompile(file);

		cout << "\033[92mCompiling...\033[0m" << '\n';

		const auto ast = parse(src);
		const auto assembly = compile(ast.get());

		ofstream stream("out.wa", ios::trunc);
		stream << assembly;
		stream.flush();

		cout << "\033[92;1mDone!\033[0m" << '\n';
	} catch(const exception& e) {
		cerr << e.what() << '\n';
	}
}

static void compile_to_bytecode(const string& file)
{
	try {
		cout << "\033[92mPrecompiling...\033[0m" << '\n';

		const auto src = precompile(file);

		cout << "\033[92mCompiling...\033[0m" << '\n';

		const auto ast = parse(src);
		const auto assembly = compile(ast.get());

		cout << "\033[92mAssembling...\033[0m" << '\n';

		Assembler assembler(assembly);

		const Program program = assembler.assemble();
		program.write("out.wp");

		cout << "\033[92;1mDone!\033[0m" << '\n';
	} catch(const exception& e) {
		cerr << e.what() << '\n';
	}
}

static void assemble_program(const string& file)
{
	const auto source = read_file(file);

	cout << "\033[92mAssembling...\033[0m" << '\n';

	Assembler assembler(source);

	try {
		const Program program = assembler.assemble();
		program.write("out.wp");

		cout << "\033[92;1mDone!\033[0m" << '\n';
	} catch(const exception& e) {
		cerr << e.what() << '\n';
	}
}

static void run_program(const string& file, const flag_t& flags)
{
	VM vm;

	try {
		vm.flags = flags;

		if(!vm.load_library("./std.so")) return;

		Program program;
		program.read(file);

		vm.bind_program(program);
		vm.prepare();
		vm.run();
	} catch(const exception& e) {
		vm.crash(e.what());
	}
}

using namespace std;

int main(int argc, char** argv)
{
	using namespace Tungsten;

	if(argc == 1) {
		show_help();
	} else if(argc >= 3) {
		const string option(argv[1]);
		const string file(argv[2]);

		if(option == "-pc") {
			precompile_program(file);
		} else if(option == "-st") {
			print_syntax_tree(file);
		} else if(option == "-ca") {
			compile_to_assembly(file);
		} else if(option == "-cb") {
			compile_to_bytecode(file);
		} else if(option == "-a") {
			assemble_program(file);
		} else if(option == "-r") {
			flag_t flags = EMPTY_F;

			if(argc >= 4) {
				const string f(argv[3]);

				if(f.find('d') != string::npos) flags |= DEBUG_F;
				if(f.find('i') != string::npos) flags |= INSTRUCTIONS_F;
			}

			run_program(file, flags);
		} else {
			show_help();
		}
	} else {
		show_help();
	}

	return 0;
}
