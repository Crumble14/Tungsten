#include "parser.hpp"

using namespace Tungsten;

void Tungsten::parse_asm(AST* ast, const char*& src)
{
	while(true) {
		skip_spaces(src);
		if(*src == SCOPE_CLOSE) break;
		if(*src == '\0') break;

		string inst;

		while(*src && *src != '\n')
		{
			inst += *src;
			++src;
		}

		ast->scope_add<ASTASM>(inst);
	}
}
