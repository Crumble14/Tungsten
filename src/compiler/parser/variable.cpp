#include "parser.hpp"

using namespace Tungsten;

void Tungsten::parse_variable(AST* ast, const char*& src, const bool argument)
{
	Keyword access_spec;
	Keyword keyword;
	Type type;
	get_keywords(ast->get_type(), src, access_spec, keyword, type);

	if(keyword != VOID_K) ERR_UNEXPECTED_KEYWORD(keyword);

	const auto name = get_name(src);
	const Variable var(access_spec, type, name);
	const auto& s = (argument ? ast->children_add<ASTVariable>(var, argument)
		: ast->scope_add<ASTVariable>(var, argument));
	parse_variable_definition(s, src, argument);
}

void Tungsten::parse_variable_definition(ASTVariable* ast,
	const char*& src, const bool arg)
{
	skip_spaces(src);

	if(check_operator(src)) {
		const auto op = get_operator(src);
		if(op != ASSIGN_O) ERR_UNEXPECTED_OPERATOR(op);

		parse_child_expression(ast, src);
	} else if(ast->get_variable().type.constant && !arg) {
		ERR_CONSTANT_NOT_INIT();
	}
}

void Tungsten::parse_function_args(ASTFunction* ast, const char*& src)
{
	while(true) {
		skip_spaces(src);
		if(*src == BRACKET_CLOSE) break;
		if(*src == '\0') ERR_UEOF();

		parse_variable(ast, src, true);

		skip_spaces(src);
		if(*src != COMMA) break;
		++src;
	}
}
