#include "parser.hpp"

using namespace Tungsten;

void Tungsten::parse_if(AST* ast, const char*& src)
{
	parse_bracket(ast, src, parse_child_expression);
	parse_scope(ast, src, parse_instructions);

	skip_spaces(src);
	if(!check_keyword(src)) return;

	const auto keyword = peek_keyword(src);
	if(keyword != ELSE_K) return;

	get_keyword(src);
	skip_spaces(src);

	auto s = ast->children_add<ASTElse>();

	if(check_keyword(src)) {
		const auto k = get_keyword(src);
		if(k != IF_K) ERR_UNEXPECTED_KEYWORD(k);

		parse_if(s->children_add<ASTIf>(), src);
	} else if(*src == SCOPE_OPEN) {
		parse_scope(s, src, parse_instructions);
	} else {
		ERR_SYNTAX();
	}
}
