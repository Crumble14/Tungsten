#include "parser.hpp"

using namespace Tungsten;

void Tungsten::parse_while(AST* ast, const char*& src)
{
	parse_bracket(ast, src, parse_child_expression);
	parse_scope(ast, src, parse_instructions);
}

void Tungsten::parse_do_while(AST* ast, const char*& src)
{
	parse_scope(ast, src, parse_instructions);

	const auto keyword = get_keyword(src);

	if(!check_keyword(src) || keyword != WHILE_K) {
		ERR_UNEXPECTED_KEYWORD(keyword);
	}

	parse_bracket(ast->children_add<ASTWhile>(), src, parse_child_expression);
}
