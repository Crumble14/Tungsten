#include "parser.hpp"

using namespace Tungsten;

void parse_switch_value(AST* ast, const char*& src)
{
	skip_spaces(src);

	if(is_numeric(*src)) {
		const auto value = parse_int(src);
		ast->children_add<ASTValue>(value, sizeof(value));
	} else if(*src == CHAR_DELIMITER) {
		const auto value = parse_char(src);
		ast->children_add<ASTValue>(value, sizeof(value));
	} else {
		ERR_SWITCH_VALUE();
	}
}

void Tungsten::parse_switch(AST* ast, const char*& src)
{
	while(true) {
		skip_spaces(src);
		if(*src == SCOPE_CLOSE) break;
		if(*src == '\0') break;

		const auto keyword = get_keyword(src);

		switch(keyword) {
			case CASE_K: {
				const auto& s = ast->scope_add<ASTCase>();
				parse_bracket(s, src, parse_switch_value);
				parse_scope(s, src, parse_instructions);

				break;
			}

			case DEFAULT_K: {
				parse_scope(ast->scope_add<ASTDefault>(), src,
					parse_instructions);
				break;
			}

			default: ERR_UNEXPECTED_KEYWORD(keyword);
		}
	}
}
