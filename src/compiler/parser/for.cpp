#include "parser.hpp"

using namespace Tungsten;

static void parse_for_arguments(AST* ast, const char*& src)
{
	parse_variable(ast, src, true);
	expect(src, COMMA);
	parse_child_expression(ast, src);
	expect(src, COMMA);
	parse_child_expression(ast, src);
}

void Tungsten::parse_for(AST* ast, const char*& src)
{
	parse_bracket(ast, src, parse_for_arguments);
	parse_scope(ast, src, parse_instructions);
}
