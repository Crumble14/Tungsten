#include "ast.hpp"
#include "../compiler.hpp"

using namespace Tungsten;

static Opcode operator_to_opcode(const Operator op)
{
	switch(op) {
		case ADD_O: return ADD;
		case SUB_O: return SUB;
		case MUL_O: return MUL;
		case DIV_O: return DIV;
		case MOD_O: return MOD;
		case POW_O: return POW;
		case INCREMENT_O: return INCR;
		case DECREMENT_O: return DECR;
		case NOT_O: return NOT;
		case AND_O: return AND;
		case OR_O: return OR;
		case XOR_O: return XOR;
		case LEFT_SHIFT_O: return LEFT;
		case RIGHT_SHIFT_O: return RIGHT;
		case EQUALS_O: return EQ;
		case NOT_EQUALS_O: return NEQ;
		case LOWER_O: return LOW;
		case LOWER_OR_EQUALS_O: return LOWE;
		case HIGHER_O: return HIGH;
		case HIGHER_OR_EQUALS_O: return HIGHE;
		// TODO
		case INVERSE_O: return INV;
		// TODO
		default: {
			throw compile_exception("Internal error!");
		}
	}
}

void ASTOperator::compile(CompileData& data) const
{
	const string inst(get_opcode(operator_to_opcode(op)));
	data.assembly.add_inst(inst + std::to_string(data.expr_data_size), true);
}

string ASTOperator::to_string() const
{
	return "Operator `"s + Tungsten::get_operator(op) + '`';
}
