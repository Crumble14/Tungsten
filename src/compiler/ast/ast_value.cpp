#include "ast.hpp"
#include "../compiler.hpp"

using namespace Tungsten;

void ASTValue::compile(CompileData& data) const
{
	if(size != 1 && size != 2 && size != 4 && size != 8) ERR_INTERNAL();

	const auto s = (data.expr_data_size == 0 ? size : data.expr_data_size);
	data.assembly.add_inst("push"s + std::to_string(s)
		+ ' ' + std::to_string(value), true);
}

string ASTValue::to_string() const
{
	return "Numeric value: `"s + std::to_string(value) + "`";
}
