#include "ast.hpp"

using namespace Tungsten;

void Asm::merge(const Asm assembly)
{
	lines.reserve(lines.size() + assembly.lines.size());

	for(const auto& l : assembly.lines) {
		lines.push_back(l);
	}

	insts += assembly.insts;
}

string Asm::dump() const
{
	size_t total_length = 0;

	for(const auto& l : lines) {
		total_length += l.size() + 1;
	}

	string str;
	str.reserve(total_length + 1);

	for(const auto& l : lines) {
		str += l;
		str += '\n';
	}

	return str;
}
