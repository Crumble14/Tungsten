#include "ast.hpp"

using namespace Tungsten;

void ASTReturn::compile(CompileData& data) const
{
	// TODO Cast to function return type
	for(const auto& c : children) c->compile(data);
	data.assembly.add_inst("ret", true);
}

string ASTReturn::to_string() const
{
	return "Return statement";
}
