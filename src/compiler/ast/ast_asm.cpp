#include "ast.hpp"

using namespace Tungsten;

void ASTASM::compile(CompileData& data) const
{
	data.assembly.add_inst(inst, true);
}

string ASTASM::to_string() const
{
	return "Assembly instruction `"s + inst + '`';
}
