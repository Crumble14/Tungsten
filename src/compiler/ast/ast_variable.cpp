#include "ast.hpp"
#include "../compiler.hpp"

using namespace Tungsten;

void ASTVariable::compile(CompileData& data) const
{
	data.variables.back().push_back(variable);
	const auto size = variable.type.get_size();
	const auto position = data.get_variable_position(variable.name);

	// TODO Handle global variables
	data.assembly.add_inst("vcreate"s + std::to_string(size), true);

	if(argument) {
		// TODO Handle default values
	} else if(!children.empty()) {
		const auto& s = children.front();
		s->compile(data);
		cast(data, get_data_size(data, s.get()), size);
	}

	if(argument || !children.empty()) {
		data.assembly.add_inst("vstore"s + std::to_string(size)
			+ ' ' + std::to_string(position), true);
	}
}

string ASTVariable::to_string() const
{
	return "Variable `"s + variable.name + "`";
}
