#include "ast.hpp"
#include "../compiler.hpp"

using namespace Tungsten;

void ASTWhile::compile(CompileData& data) const
{
	if(children.size() != 1) ERR_INTERNAL();
	if(children.front()->get_type() != EXPRESSION_S) ERR_INTERNAL();

	const auto begin = data.assembly.insts;

	children.front()->compile(data);

	const auto size = get_data_size(data, children.front().get());
	if(size != 1) cast(data, size, 1);

	CompileData d(data.fork());

	for(const auto& s : scope)
	{
		s->compile(d);
		cancel_stack_value(d, s.get());
	}

	const auto jump = data.assembly.insts + d.assembly.insts + 3;
	data.assembly.add_inst("inv1", true);
	data.assembly.add_inst("ifjmp "s + std::to_string(jump), true);
	data.merge(d);
	data.assembly.add_inst("jmp "s + std::to_string(begin), true);
}

string ASTWhile::to_string() const
{
	return "While loop";
}
