#include "ast.hpp"
#include "../compiler.hpp"

using namespace Tungsten;

void ASTExpression::compile(CompileData& data) const
{
	uint8_t prev_size = 0;

	for(const auto& c : children) {
		if(prev_size == 0) prev_size = get_data_size(data, c.get());
		data.expr_data_size = prev_size;

		if(c->get_type() == OPERATOR_S) {
			cast(data, prev_size, data.expr_data_size);
			c->compile(data);

			const auto s = get_data_size(data, c.get());
			if(s != 0) prev_size = s;
		} else {
			cast(data, get_data_size(data, c.get()), data.expr_data_size);
			c->compile(data);
		}
	}
}

string ASTExpression::to_string() const
{
	return "Expression";
}
