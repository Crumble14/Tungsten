#ifndef AST_HPP
# define AST_HPP

# include <algorithm>
# include <memory>
# include <string>
# include <vector>

# include "../../tungsten.hpp"

namespace Tungsten
{
	using namespace std;

	enum Keyword : uint8_t
	{
		VOID_K = 0,
		INT8_K,
		UINT8_K,
		INT16_K,
		UINT16_K,
		INT32_K,
		UINT32_K,
		INT64_K,
		UINT64_K,
		FLOAT_K,
		DOUBLE_K,
		BOOL_K,
		FALSE_K,
		TRUE_K,
		NULL_K,
		CONST_K,
		CONSTEXPR_K,
		NAMESPACE_K,
		PRIVATE_K,
		PROTECTED_K,
		PUBLIC_K,
		INLINE_K,
		RETURN_K,
		IF_K,
		ELSE_K,
		SWITCH_K,
		CASE_K,
		DEFAULT_K,
		WHILE_K,
		FOR_K,
		DO_K,
		CONTINUE_K,
		BREAK_K,
		TRY_K,
		CATCH_K,
		THROW_K,
		STRUCT_K,
		THIS_K,
		SUPER_K,
		OPERATOR_K,
		NEW_K,
		DELETE_K,
		TEMPLATE_K,
		SIZEOF_K,
		ASM_K
	};

	enum Operator : uint8_t
	{
		ADD_O = 0,
		SUB_O,
		MUL_O,
		DIV_O,
		MOD_O,
		POW_O,
		INCREMENT_O,
		DECREMENT_O,
		NOT_O,
		AND_O,
		OR_O,
		XOR_O,
		LEFT_SHIFT_O,
		RIGHT_SHIFT_O,
		ASSIGN_O,
		ADD_ASSIGN_O,
		SUB_ASSIGN_O,
		MUL_ASSIGN_O,
		DIV_ASSIGN_O,
		MOD_ASSIGN_O,
		POW_ASSIGN_O,
		EQUALS_O,
		NOT_EQUALS_O,
		LOWER_O,
		LOWER_OR_EQUALS_O,
		HIGHER_O,
		HIGHER_OR_EQUALS_O,
		TERNARY_CONDITION_O,
		TERNARY_ELSE_O,
		INVERSE_O,
		FUNCTION_CALL_O,
		ARRAY_LOAD_O,
		MEMBER_LOAD_O
	};

	enum StatementType : uint8_t
	{
		ROOT_S = 0,
		VARIABLE_S,
		EXPRESSION_S,
		VALUE_S,
		STRING_S,
		OPERATOR_S,
		ADDRESS_OF_S,
		INDIRECTION_S,
		VAR_LOAD_S,
		VAR_INCREMENT_S,
		VAR_DECREMENT_S,
		FUNCTION_CALL_S,
		ARRAY_LOAD_S,
		MEMBER_LOAD_S,
		FUNCTION_S,
		NAMESPACE_S,
		RETURN_S,
		IF_S,
		ELSE_S,
		SWITCH_S,
		CASE_S,
		DEFAULT_S,
		WHILE_S,
		FOR_S,
		DO_S,
		CONTINUE_S,
		BREAK_S,
		TRY_S,
		CATCH_S,
		THROW_S,
		STRUCT_S,
		THIS_S,
		SUPER_S,
		NEW_S,
		FREE_S,
		TEMPLATE_S,
		SIZEOF_S,
		ASM_S
	};

	struct CompileData;

	struct Asm
	{
		vector<string> lines;
		size_t insts = 0;

		Asm() = default;

		inline Asm(const size_t insts)
			: insts{insts}
		{}

		inline void add_line(const string line)
		{
			lines.push_back(line);
		}

		inline void add_line()
		{
			lines.emplace_back();
		}

		inline void add_inst(const string line, const bool in_function)
		{
			add_line(in_function ? "\t"s + line : line);
			++insts;
		}

		void merge(const Asm assembly);

		string dump() const;
	};

	struct Type
	{
		bool constant = false;
		Keyword type = VOID_K;
		size_t pointer = false;
		bool reference = false;

		Type() = default;

		inline Type(const bool constant, const Keyword type,
			const size_t pointer, const bool reference)
			: constant{constant}, type{type},
				pointer{pointer}, reference{reference}
		{}

		uint8_t get_size(const bool ignore_ptr = false) const;
	};

	Keyword get_default_access(const StatementType type);

	struct Variable
	{
		Keyword access = get_default_access(VARIABLE_S);
		Type type;
		string name;

		inline Variable(const Type& type, const string& name)
			: type{type}, name{name}
		{}

		inline Variable(const Keyword access, const Type& type,
			const string& name)
			: access{access}, type{type}, name{name}
		{}
	};

	struct Function
	{
		bool defined;

		Keyword access = get_default_access(FUNCTION_S);
		bool constant;
		Type return_type;
		string ns;
		string name;

		inline Function(const bool constant, const Type return_type,
			const string& name)
			: constant{constant}, return_type{return_type}, name{name}
		{}

		inline Function(const Keyword access, const bool constant,
			const Type return_type, const string& name)
			: access{access}, constant{constant},
			return_type{return_type}, name{name}
		{}

		string get_asm_name() const;
	};

	struct CompileData
	{
		Asm assembly;
		vector<string> namespaces;
		vector<vector<Variable>> variables;
		vector<Function> functions;

		uint8_t expr_data_size = 0;

		string get_namespace_asm() const;

		const Variable& get_variable(const string& name) const;
		uint8_t get_variable_position(const string& name) const;

		const Function& get_function(const string& name) const;

		// TODO Add arguments matching
		bool has_function(const string name) const;

		inline bool has_main() const
		{
			return has_function("main");
		}

		inline CompileData fork() const
		{
			CompileData data(*this);
			data.assembly = Asm();

			return data;
		}

		inline void merge(const CompileData& data)
		{
			assembly.merge(data.assembly);
		}

		inline string dump() const
		{
			return assembly.dump();
		}
	};

	class AST
	{
		public:
			inline AST(const StatementType type)
				: type{type}
			{}

			inline StatementType get_type() const
			{
				return type;
			}

			inline const vector<unique_ptr<AST>>& get_children() const
			{
				return children;
			}

			inline const vector<unique_ptr<AST>>& get_scope() const
			{
				return scope;
			}

			template<typename T, typename ... Args>
			inline T* children_add(Args... args)
			{
				auto s = new T(args...);
				children.emplace_back(s);

				return s;
			}

			template<typename T, typename ... Args>
			inline T* scope_add(Args... args)
			{
				auto s = new T(args...);
				scope.emplace_back(s);

				return s;
			}

			virtual void compile(CompileData& data) const = 0;
			virtual string to_string() const = 0;

		protected:
			StatementType type;

			vector<unique_ptr<AST>> children;
			vector<unique_ptr<AST>> scope;
	};

	class ASTRoot : public AST
	{
		public:
			inline ASTRoot()
				: AST(ROOT_S)
			{}

			void compile(CompileData& data) const override;
			string to_string() const override;
	};

	class ASTVariable : public AST
	{
		public:
			inline ASTVariable(const Variable& variable, const bool argument)
				: AST(VARIABLE_S), variable{variable}, argument{argument}
			{}

			inline const Variable& get_variable() const
			{
				return variable;
			}

			inline bool is_argument() const
			{
				return argument;
			}

			void compile(CompileData& data) const override;
			string to_string() const override;

		private:
			Variable variable;
			bool argument;
	};

	class ASTExpression : public AST
	{
		public:
			inline ASTExpression()
				: AST(EXPRESSION_S)
			{}

			void compile(CompileData& data) const override;
			string to_string() const override;
	};

	class ASTValue : public AST
	{
		public:
			template<typename T>
			inline ASTValue(const T& value)
				: AST(VALUE_S), value(value), size{sizeof(T)}
			{}

			template<typename T>
			inline ASTValue(const T& value, const size_t size)
				: AST(VALUE_S), value(value), size{size}
			{}

			inline uint64_t get_value() const
			{
				return value;
			}

			inline size_t get_size() const
			{
				return size;
			}

			void compile(CompileData& data) const override;
			string to_string() const override;

		private:
			uint64_t value;
			size_t size;
	};

	class ASTString : public AST
	{
		public:
			inline ASTString(const string& str)
				: AST(STRING_S), str{str}
			{}

			inline const string& get_string() const
			{
				return str;
			}

			void compile(CompileData& data) const override;
			string to_string() const override;

		private:
			string str;
	};

	class ASTOperator : public AST
	{
		public:
			inline ASTOperator(const Operator op)
				: AST(OPERATOR_S), op{op}
			{}

			inline Operator get_operator() const
			{
				return op;
			}

			void compile(CompileData& data) const override;
			string to_string() const override;

		private:
			Operator op;
	};

	class ASTAddressOf : public AST
	{
		public:
			inline ASTAddressOf(const string& variable)
				: AST(ADDRESS_OF_S), variable{variable}
			{}

			inline const string& get_variable() const
			{
				return variable;
			}

			void compile(CompileData& data) const override;
			string to_string() const override;

		private:
			string variable;
	};

	class ASTIndirection : public AST
	{
		public:
			inline ASTIndirection()
				: AST(INDIRECTION_S)
			{}

			void compile(CompileData& data) const override;
			string to_string() const override;
	};

	class ASTVarLoad : public AST
	{
		public:
			inline ASTVarLoad(const string& variable)
				: AST(VAR_LOAD_S), variable{variable}
			{}

			inline const string& get_variable() const
			{
				return variable;
			}

			void compile(CompileData& data) const override;
			string to_string() const override;

		private:
			string variable;
	};

	class ASTVarIncrement : public AST
	{
		public:
			inline ASTVarIncrement(const string& variable)
				: AST(VAR_INCREMENT_S), variable{variable}
			{}

			inline const string& get_variable() const
			{
				return variable;
			}

			void compile(CompileData& data) const override;
			string to_string() const override;

		private:
			string variable;
	};

	class ASTVarDecrement : public AST
	{
		public:
			inline ASTVarDecrement(const string& variable)
				: AST(VAR_DECREMENT_S), variable{variable}
			{}

			inline const string& get_variable() const
			{
				return variable;
			}

			void compile(CompileData& data) const override;
			string to_string() const override;

		private:
			string variable;
	};

	class ASTFunctionCall : public AST
	{
		public:
			inline ASTFunctionCall(const string& function)
				: AST(FUNCTION_CALL_S), function{function}
			{}

			inline const string& get_function() const
			{
				return function;
			}

			void compile(CompileData& data) const override;
			string to_string() const override;

		private:
			string function;
	};

	class ASTArrayLoad : public AST
	{
		public:
			inline ASTArrayLoad(const string& variable)
				: AST(ARRAY_LOAD_S), variable{variable}
			{}

			inline const string& get_variable() const
			{
				return variable;
			}

			void compile(CompileData& data) const override;
			string to_string() const override;

		private:
			string variable;
	};

	class ASTMemberLoad : public AST
	{
		public:
			inline ASTMemberLoad()
				: AST(MEMBER_LOAD_S)
			{}

			void compile(CompileData& data) const override;
			string to_string() const override;
	};

	class ASTFunction : public AST
	{
		public:
			inline ASTFunction(const Function& function)
				: AST(FUNCTION_S), function{function}
			{}

			inline const Function& get_function() const
			{
				return function;
			}

			void compile(CompileData& data) const override;
			string to_string() const override;

		private:
			Function function;
	};

	class ASTNamespace : public AST
	{
		public:
			inline ASTNamespace(const string& name)
				: AST(NAMESPACE_S), name{name}
			{}

			inline ASTNamespace(const Keyword access, const string& name)
				: AST(NAMESPACE_S), access{access}, name{name}
			{}

			inline Keyword get_access() const
			{
				return access;
			}

			inline const string& get_name() const
			{
				return name;
			}

			void compile(CompileData& data) const override;
			string to_string() const override;

		private:
			Keyword access = get_default_access(NAMESPACE_S);
			string name;
	};

	class ASTReturn : public AST
	{
		public:
			inline ASTReturn()
				: AST(RETURN_S)
			{}

			void compile(CompileData& data) const override;
			string to_string() const override;
	};

	class ASTIf : public AST
	{
		public:
			inline ASTIf()
				: AST(IF_S)
			{}

			void compile(CompileData& data) const override;
			string to_string() const override;
	};

	class ASTElse : public AST
	{
		public:
			inline ASTElse()
				: AST(ELSE_S)
			{}

			void compile(CompileData& data) const override;
			string to_string() const override;
	};

	class ASTSwitch : public AST
	{
		public:
			inline ASTSwitch()
				: AST(SWITCH_S)
			{}

			void compile(CompileData& data) const override;
			string to_string() const override;
	};

	class ASTCase : public AST
	{
		public:
			inline ASTCase()
				: AST(CASE_S)
			{}

			void compile(CompileData& data) const override;
			string to_string() const override;
	};

	class ASTDefault : public AST
	{
		public:
			inline ASTDefault()
				: AST(DEFAULT_S)
			{}

			void compile(CompileData& data) const override;
			string to_string() const override;
	};

	class ASTWhile : public AST
	{
		public:
			inline ASTWhile()
				: AST(WHILE_S)
			{}

			void compile(CompileData& data) const override;
			string to_string() const override;
	};

	class ASTFor : public AST
	{
		public:
			inline ASTFor()
				: AST(FOR_S)
			{}

			void compile(CompileData& data) const override;
			string to_string() const override;
	};

	class ASTDo : public AST
	{
		public:
			inline ASTDo()
				: AST(DO_S)
			{}

			void compile(CompileData& data) const override;
			string to_string() const override;
	};

	class ASTContinue : public AST
	{
		public:
			inline ASTContinue()
				: AST(CONTINUE_S)
			{}

			void compile(CompileData& data) const override;
			string to_string() const override;
	};

	class ASTBreak : public AST
	{
		public:
			inline ASTBreak()
				: AST(BREAK_S)
			{}

			void compile(CompileData& data) const override;
			string to_string() const override;
	};

	class ASTTry : public AST
	{
		public:
			inline ASTTry()
				: AST(TRY_S)
			{}

			void compile(CompileData& data) const override;
			string to_string() const override;
	};

	class ASTCatch : public AST
	{
		public:
			inline ASTCatch()
				: AST(CATCH_S)
			{}

			void compile(CompileData& data) const override;
			string to_string() const override;
	};

	class ASTThrow : public AST
	{
		public:
			inline ASTThrow()
				: AST(THROW_S)
			{}

			void compile(CompileData& data) const override;
			string to_string() const override;
	};

	class ASTStruct : public AST
	{
		public:
			inline ASTStruct()
				: AST(STRUCT_S)
			{}

			void compile(CompileData& data) const override;
			string to_string() const override;
	};

	class ASTThis : public AST
	{
		public:
			inline ASTThis()
				: AST(THIS_S)
			{}

			void compile(CompileData& data) const override;
			string to_string() const override;
	};

	class ASTSuper : public AST
	{
		public:
			inline ASTSuper()
				: AST(SUPER_S)
			{}

			void compile(CompileData& data) const override;
			string to_string() const override;
	};

	class ASTNew : public AST
	{
		public:
			inline ASTNew()
				: AST(NEW_S)
			{}

			void compile(CompileData& data) const override;
			string to_string() const override;
	};

	class ASTFree : public AST
	{
		public:
			inline ASTFree()
				: AST(FREE_S)
			{}

			void compile(CompileData& data) const override;
			string to_string() const override;
	};

	class ASTTemplate : public AST
	{
		public:
			inline ASTTemplate()
				: AST(TEMPLATE_S)
			{}

			void compile(CompileData& data) const override;
			string to_string() const override;
	};

	class ASTSizeof : public AST
	{
		public:
			inline ASTSizeof()
				: AST(SIZEOF_S)
			{}

			void compile(CompileData& data) const override;
			string to_string() const override;
	};

	class ASTASM : public AST
	{
		public:
			inline ASTASM(const string& inst)
				: AST(ASM_S), inst{inst}
			{}

			inline const string& get_inst() const
			{
				return inst;
			}

			void compile(CompileData& data) const override;
			string to_string() const override;

		private:
			string inst;
	};
}

#endif
