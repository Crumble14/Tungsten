#include "ast.hpp"

using namespace Tungsten;

void ASTRoot::compile(CompileData& data) const
{
	data.assembly.add_inst("call main", false);
	data.assembly.add_inst("exit", false);

	data.variables.emplace_back();

	for(const auto& s : scope) s->compile(data);

	data.variables.pop_back();
}

string ASTRoot::to_string() const
{
	return "Program";
}
