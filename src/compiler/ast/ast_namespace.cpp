#include "ast.hpp"

using namespace Tungsten;

void ASTNamespace::compile(CompileData& data) const
{
	data.namespaces.push_back(name);

	for(const auto& s : scope) s->compile(data);

	data.namespaces.pop_back();
}

string ASTNamespace::to_string() const
{
	return "Namespace";
}
