#include "ast.hpp"
#include "../compiler.hpp"

using namespace Tungsten;

uint8_t Type::get_size(const bool ignore_ptr) const
{
	if(!ignore_ptr && pointer) return 8;

	switch(type) {
		case INT8_K: return 1;
		case UINT8_K: return 1;
		case INT16_K: return 2;
		case UINT16_K: return 2;
		case INT32_K: return 4;
		case UINT32_K: return 4;
		case INT64_K: return 8;
		case UINT64_K: return 8;
		case FLOAT_K: return 4;
		case DOUBLE_K: return 8;
		case BOOL_K: return 1;
		default: return 0;
	}
}
