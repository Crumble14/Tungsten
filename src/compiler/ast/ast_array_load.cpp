#include "ast.hpp"
#include "../compiler.hpp"

using namespace Tungsten;

void ASTArrayLoad::compile(CompileData& data) const
{
	if(children.size() != 1) ERR_INTERNAL();

	const auto var = data.get_variable(variable);
	const auto size = var.type.get_size();

	data.assembly.add_inst("vload"s + std::to_string(size)
		+ ' ' + std::to_string(data.get_variable_position(variable)), true);
	cast(data, size, 8);

	children.front()->compile(data);
	cast(data, get_data_size(data, children.front().get()), 8);

	const auto value_size = var.type.get_size(true);

	data.assembly.add_inst("push8 "s
		+ std::to_string(value_size), true);
	data.assembly.add_inst("mul8", true);
	data.assembly.add_inst("add8", true);
	data.assembly.add_inst("mload"s + std::to_string(value_size), true);
}

string ASTArrayLoad::to_string() const
{
	return "Array load from variable `"s + variable + '`';
}
