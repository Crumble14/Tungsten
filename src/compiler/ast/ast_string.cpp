#include "ast.hpp"

using namespace Tungsten;

void ASTString::compile(CompileData& data) const
{
	data.assembly.add_inst("cpush \""s + str + '"', true);
}

string ASTString::to_string() const
{
	return "String value";
}
