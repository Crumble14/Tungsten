#include "ast.hpp"
#include "../compiler.hpp"

using namespace Tungsten;

void ASTElse::compile(CompileData& data) const
{
	if(children.size() > 1) ERR_INTERNAL();

	if(!children.empty()) {
		const auto& s = children.front();

		if(s->get_type() != IF_S) ERR_INTERNAL();
		s->compile(data);
	}

	for(const auto& s : scope) s->compile(data);
}

string ASTElse::to_string() const
{
	return "Else block";
}
