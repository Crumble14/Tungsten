#include "ast.hpp"

using namespace Tungsten;

void ASTAddressOf::compile(CompileData& data) const
{
	data.get_variable(variable);
	data.assembly.add_inst("vptr "s
		+ std::to_string(data.get_variable_position(variable)), true);
}

string ASTAddressOf::to_string() const
{
	return "Address of variable `"s + variable + "`";
}
