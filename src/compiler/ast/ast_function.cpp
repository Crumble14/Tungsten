#include "ast.hpp"
#include "../compiler.hpp"

using namespace Tungsten;

void ASTFunction::compile(CompileData& data) const
{
	data.functions.push_back(function);
	data.functions.back().ns = data.get_namespace_asm();

	data.assembly.add_line();
	// TODO Namespace
	data.assembly.add_line(function.get_asm_name() + ':');

	data.variables.emplace_back();

	for(const auto& c : children) c->compile(data);

	for(const auto& s : scope) {
		s->compile(data);
		cancel_stack_value(data, s.get());
	}

	if(function.return_type.get_size() > 0) {
		// TODO If the function contains a `if`, check for return inside
		if(scope.empty() || scope.back()->get_type() != RETURN_S) {
			ERR_MUST_RETURN(function.name);
		}
	} else {
		data.assembly.add_inst("ret", true);
	}

	data.variables.pop_back();
}

string ASTFunction::to_string() const
{
	return "Function `"s + function.name + "`";
}
