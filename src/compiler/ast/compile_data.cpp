#include "ast.hpp"
#include "../compiler.hpp"

using namespace Tungsten;

string CompileData::get_namespace_asm() const
{
	string name;
	for(const auto& n : namespaces) name += n + '_';

	return name;
}

const Variable& CompileData::get_variable(const string& name) const
{
	if(variables.empty()) ERR_INTERNAL();

	for(const auto& v : variables.back()) {
		if(v.name == name) return v;
	}

	ERR_VAR_NOT_FOUND(name);
}

uint8_t CompileData::get_variable_position(const string& name) const
{
	uint8_t i = 0;

	for(const auto& v : variables.back()) {
		if(v.name == name) return i;
		i += v.type.get_size();
	}

	ERR_VAR_NOT_FOUND(name);
}

const Function& CompileData::get_function(const string& name) const
{
	// TODO Namespaces
	for(const auto& f : functions) {
		if(f.name == name) return f;
	}

	ERR_FUNC_NOT_FOUND(name);
}

bool CompileData::has_function(const string name) const
{
	return any_of(functions.cbegin(), functions.cend(),
		[this, &name](const Function& f)
		{
			return (f.get_asm_name() == name);
		});
}
