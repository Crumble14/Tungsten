#ifndef COMPILER_HPP
# define COMPILER_HPP

# include "../tungsten.hpp"
# include "error.hpp"
# include "ast/ast.hpp"
# include "parser/parser.hpp"

# include <exception>
# include <fstream>
# include <iostream>
# include <memory>
# include <string>
# include <unordered_map>
# include <utility>
# include <vector>

namespace Tungsten
{
	using namespace std;

	string read_file(const string& file);

	constexpr const char* PC_FILE = "// _PC_FILE:";
	constexpr const char* PC_END = "// _PC_END";

	struct PrecompileData
	{
		const string current_folder;

		unordered_map<string, string> macros;
		bool backslash = false;
		bool in_string = false;

		string output;

		inline PrecompileData(const string& current_folder)
			: current_folder{current_folder}
		{}
	};

	string precompile(const string file);

	class compile_exception : public exception
	{
		public:
			inline compile_exception(const string message)
				: message{message}
			{}

			inline const string& get_message() const
			{
				return message;
			}

			inline virtual const char* what() const noexcept
			{
				return message.c_str();
			}

		private:
			const string message;
	};

	string get_function_asm_name(const string& ns, const string& function);

	uint8_t get_data_size(const CompileData& data, const AST* ast);
	void cast(CompileData& data, const uint8_t from, const uint8_t to);
	void cancel_stack_value(CompileData& data, const AST* ast);

	string compile(const AST* ast);
}

#endif
