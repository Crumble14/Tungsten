#ifndef TUNGSTEN_HPP
# define TUNGSTEN_HPP

# include <array>
# include <cmath>
# include <cstring>
# include <exception>
# include <fstream>
# include <functional>
# include <iomanip>
# include <iostream>
# include <mutex>
# include <sstream>
# include <stdexcept>
# include <stdint.h>
# include <string>
# include <unordered_map>
# include <vector>

# include <dlfcn.h>
# include <pthread.h>

# define VERSION "0.1"

namespace Tungsten
{
	using namespace std;

	class Thread;
	class VM;

	typedef uint8_t flag_t;
	typedef function<void(VM&, Thread&)> native_t;

	enum Opcode : uint8_t
	{
		PUSH = 0x00,
		CPUSH = 0x01,
		POP = 0x02,
		DUP = 0x03,

		ADD = 0x04,
		SUB = 0x05,
		MUL = 0x06,
		DIV = 0x07,
		MOD = 0x08,
		POW = 0x09,
		INCR = 0x0a,
		DECR = 0x0b,
		NEG = 0x0c,
		INV = 0x0d,

		NOT = 0x0e,
		AND = 0x0f,
		OR = 0x10,
		XOR = 0x11,
		LEFT = 0x12,
		RIGHT = 0x13,

		EQ = 0x14,
		NEQ = 0x15,
		LOW = 0x16,
		LOWE = 0x17,
		HIGH = 0x18,
		HIGHE = 0x19,

		JMP = 0x1a,
		IFJMP = 0x1b,
		CALL = 0x1c,
		RET = 0x1d,

		VCREATE = 0x1e,
		VSTORE = 0x1f,
		VLOAD = 0x20,
		VPTR = 0x21,
		MSTORE = 0x22,
		MLOAD = 0x23,

		NCALL = 0x24,
		EXIT = 0x25
	};

	const char* get_opcode(const Opcode opcode);
	Opcode get_opcode(const char* str);

	template<typename T>
	string get_hexa(const T n)
	{
		static const char* hex = "0123456789abcdef";
		string str;

		if(n > 15) str = get_hexa(n / 16);
		return str + hex[n % 16];
	}

	enum ConstantType : uint8_t
	{
		NATIVE_C = 0x00,
		STRING_C = 0x01
	};

	struct Constant
	{
		ConstantType type;
		string value;

		inline Constant(const ConstantType type, const string& value)
			: type{type}, value{value}
		{}
	};

	struct Instruction
	{
		Opcode opcode;
		uint8_t size;
		uint32_t args;

		Instruction() = default;

		inline Instruction(const Opcode opcode, const uint8_t size,
			const uint32_t args)
			: opcode{opcode}, size{size}, args{args}
		{}

		template<typename T>
		inline T get_arg(const uint8_t i) const
		{
			return *reinterpret_cast<const T*>(((const char*) &args) + i);
		}

		string dump() const;
	};

	struct Program
	{
		vector<Constant> constants;
		vector<Instruction> instructions;

		void read(const string& file);
		void write(const string& file) const;
	};

	class bytecode_exception : public exception
	{
		public:
			inline virtual const char* what() const noexcept
			{
				return "Ill-formed bytecode!";
			}
	};

	class Assembler
	{
		public:
			inline Assembler(const string& assembly)
				: assembly{assembly}
			{}

			Program assemble();

		private:
			const string assembly;

			unordered_map<string, uint32_t> functions;
			Program program;

			void handle_functions();
			void handle_instructions();

			uint32_t get_function_begin(const string& name);
			uint32_t get_constant_id(const ConstantType type,
				const string& name);

			void handle_string_push(const char*& str);
	};

	constexpr flag_t EMPTY_F = 0x00;
	constexpr flag_t DEBUG_F = 0x01;
	constexpr flag_t INSTRUCTIONS_F = 0x02;

	class Stack
	{
		public:
			inline Stack()
				: stack{(char*) malloc(capacity)}, top{stack}
			{}

			inline ~Stack()
			{
				free(stack);
			}

			template<typename T>
			inline void push(const T& value)
			{
				*reinterpret_cast<T*>(top) = value;
				top += sizeof(T);
			}

			template<typename T>
			inline T& peek() const
			{
				return *reinterpret_cast<T*>(top - sizeof(T));
			}

			template<typename T>
			inline T get()
			{
				const auto v = peek<T>();
				pop<T>();

				return v;
			}

			template<typename T>
			inline void pop()
			{
				top -= sizeof(T);
			}

			inline size_t size() const
			{
				return top - stack;
			}

			inline void resize(const size_t size)
			{
				top = stack + size;
			}

			inline size_t get_capacity() const
			{
				return capacity;
			}

			inline void reserve(const size_t size)
			{
				if(size <= capacity) return;

				capacity = size;
				stack = (char*) realloc((void*) stack, capacity);
			}

			string dump() const;

		private:
			size_t capacity = 524288;
			char* stack;
			char* top;
	};

	class Thread
	{
		public:
			inline Thread(const pthread_t id)
				: id{id}
			{}

			inline pthread_t get_id() const
			{
				return id;
			}

			inline bool is_current() const
			{
				return (id == pthread_self());
			}

			inline bool is_destroyed() const
			{
				return destroyed;
			}

			inline Stack& get_stack()
			{
				return stack;
			}

			inline const Stack& get_stack() const
			{
				return stack;
			}

			void call(const uint32_t i);
			uint32_t ret();

			inline Stack& get_callstack()
			{
				return callstack;
			}

			inline const Stack& get_callstack() const
			{
				return callstack;
			}

			inline size_t get_call_depth() const
			{
				return callstack.size() / sizeof(uint32_t) / 2;
			}

			template<typename T>
			inline void var_create()
			{
				vars_data.resize(vars_data.size() + sizeof(T));
			}

			template<typename T>
			inline void var_store(const uint32_t i, const T& value)
			{
				*reinterpret_cast<T*>(vars_data.data()
					+ callstack.peek<uint32_t>() + i) = value;
			}

			template<typename T>
			inline const T& var_load(const uint32_t i) const
			{
				return *reinterpret_cast<const T*>(vars_data.data()
					+ callstack.peek<uint32_t>() + i);
			}

			inline void* var_ptr(const uint32_t i)
			{
				return vars_data.data() + callstack.peek<uint32_t>() + i;
			}

			inline void destroy()
			{
				destroyed = true;
			}

		private:
			pthread_t id;

			Stack stack;
			Stack callstack;
			vector<char> vars_data;

			bool destroyed = false;
	};

	class VM
	{
		public:
			flag_t flags = EMPTY_F;

			inline ~VM()
			{
				kill();
			}

			inline bool has_flag(const flag_t flag) const
			{
				return (flags & flag);
			}

			bool load_library(const string& file);

			inline void reserve_natives(const size_t size)
			{
				natives.reserve(natives.size() + size);
			}

			inline void register_native(const string& name,
				const native_t& function)
			{
				natives[name] = function;
			}

			inline void register_native(const string&& name,
				const native_t& function)
			{
				register_native(name, function);
			}

			void bind_program(const Program& program);
			void prepare();

			void run(const uint32_t i = 0);
			void run(uint32_t i, Thread& thread);

			inline void call_native(const size_t native, Thread& thread)
			{
				prepared_natives[native](*this, thread);
			}

			inline const Constant& get_constant(const uint32_t id) const
			{
				return program->constants[id];
			}

			void crash(const string message);
			inline void kill()
			{
				killed = true;

#ifdef TUNGSTEN_VM_DEBUG
				if(has_flag(DEBUG_F)) {
					cout << "VM killed." << '\n';
				}
#endif
			}

		private:
			unordered_map<string, native_t> natives;

			const Program* program = nullptr;
			vector<native_t> prepared_natives;

			bool killed = false;
			bool crashed = false;

			mutex crash_mutex;

			void print_stack(const Thread& thread);
	};
}

#endif
