#include "../tungsten.hpp"

using namespace Tungsten;

void Thread::call(const uint32_t i)
{
	callstack.push<uint32_t>(i);
	callstack.push<uint32_t>(vars_data.size());
}

uint32_t Thread::ret()
{
	callstack.pop<uint32_t>();
	return callstack.get<uint32_t>();
}
