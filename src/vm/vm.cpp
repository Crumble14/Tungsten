#include "../tungsten.hpp"

using namespace Tungsten;

bool VM::load_library(const string& file)
{
	void* handle = dlopen(file.c_str(), RTLD_LAZY);

	if(!handle) {
		crash("Failed to load library: `" + file + "`!");
		return false;
	}

	dlerror();

	void (*init)(VM&);
	*(void **) (&init) = dlsym(handle, "init");

	if(dlerror() != nullptr) {
		crash("Invalid library: `" + file + "`!");
		return false;
	}

	(*init)(*this);

	return true;
}

void VM::bind_program(const Program& program)
{
#ifdef TUNGSTEN_VM_DEBUG
	if(has_flag(DEBUG_F)) {
		cout << "Binding program..." << '\n';
		cout << "Program has " << program.instructions.size()
			<< " instructions." << '\n';
	}
#endif

	this->program = &program;
}

void VM::prepare()
{
#ifdef TUNGSTEN_VM_DEBUG
	if(has_flag(DEBUG_F)) {
		cout << "Preparing natives..." << '\n';
	}
#endif

	prepared_natives.clear();

	for(const auto& c : program->constants) {
		// TODO Find a better way (would be much better if there were no hole in prepared_natives)
		prepared_natives.resize(prepared_natives.size() + 1);
		if(c.type != NATIVE_C) continue;

		const auto& f = natives.find(c.value);

		if(f == natives.cend()) {
			crash(string("Can't find native `") + c.value + "`!");
		}

		prepared_natives.back() = f->second;

#ifdef TUNGSTEN_VM_DEBUG
		if(has_flag(DEBUG_F)) {
			cout << (prepared_natives.size() - 1) << " = " << f->first << '\n';
		}
#endif
	}

#ifdef TUNGSTEN_VM_DEBUG
	if(has_flag(DEBUG_F)) {
		cout << '\n';
	}
#endif
}

template<typename T>
void exec_instruction(VM& vm, Thread& thread,
	uint32_t& i, const Instruction& inst)
{
	auto& stack = thread.get_stack();

	switch(inst.opcode) {
		case PUSH: {
			stack.push<T>(inst.get_arg<int32_t>(0));
			break;
		}

		case CPUSH: {
			stack.push(vm.get_constant(inst.get_arg<uint32_t>(0))
				.value.c_str());
			break;
		}

		case POP: {
			stack.pop<T>();
			break;
		}

		case DUP: {
			stack.push(stack.peek<T>());
			break;
		}

		case ADD: {
			const auto v2 = stack.get<T>();
			const auto v1 = stack.get<T>();

			stack.push<T>(v1 + v2);
			break;
		}

		case SUB: {
			const auto v2 = stack.get<T>();
			const auto v1 = stack.get<T>();

			stack.push<T>(v1 - v2);
			break;
		}

		case MUL: {
			const auto v2 = stack.get<T>();
			const auto v1 = stack.get<T>();

			stack.push<T>(v1 * v2);
			break;
		}

		case DIV: {
			const auto v2 = stack.get<T>();
			const auto v1 = stack.get<T>();

			stack.push<T>(v1 / v2);
			break;
		}

		case MOD: {
			const auto v2 = stack.get<T>();
			const auto v1 = stack.get<T>();

			stack.push<T>(v1 % v2);
			break;
		}

		case POW: {
			const auto v2 = stack.get<T>();
			const auto v1 = stack.get<T>();

			stack.push<T>(pow(v1, v2));
			break;
		}

		case INCR: {
			++(stack.peek<T>());
			break;
		}

		case DECR: {
			--(stack.peek<T>());
			break;
		}

		case NEG: {
			auto& v = stack.peek<T>();
			v = -v;

			break;
		}

		case INV: {
			auto& v = stack.peek<T>();
			v = !v;

			break;
		}

		case NOT: {
			auto& v = stack.peek<T>();
			v = ~v;

			break;
		}

		case AND: {
			const auto v2 = stack.get<T>();
			const auto v1 = stack.get<T>();

			stack.push(v1 & v2);
			break;
		}

		case OR: {
			const auto v2 = stack.get<T>();
			const auto v1 = stack.get<T>();

			stack.push(v1 | v2);
			break;
		}

		case XOR: {
			const auto v2 = stack.get<T>();
			const auto v1 = stack.get<T>();

			stack.push(v1 ^ v2);
			break;
		}

		case LEFT: {
			const auto v2 = stack.get<T>();
			const auto v1 = stack.get<T>();

			stack.push(v1 << v2);
			break;
		}

		case RIGHT: {
			const auto v2 = stack.get<T>();
			const auto v1 = stack.get<T>();

			stack.push(v1 >> v2);
			break;
		}

		case EQ: {
			const auto v2 = stack.get<T>();
			const auto v1 = stack.get<T>();

			stack.push(v1 == v2);
			break;
		}

		case NEQ: {
			const auto v2 = stack.get<T>();
			const auto v1 = stack.get<T>();

			stack.push(v1 != v2);
			break;
		}

		case LOW: {
			const auto v2 = stack.get<T>();
			const auto v1 = stack.get<T>();

			stack.push(v1 < v2);
			break;
		}

		case LOWE: {
			const auto v2 = stack.get<T>();
			const auto v1 = stack.get<T>();

			stack.push(v1 <= v2);
			break;
		}

		case HIGH: {
			const auto v2 = stack.get<T>();
			const auto v1 = stack.get<T>();

			stack.push(v1 > v2);
			break;
		}

		case HIGHE: {
			const auto v2 = stack.get<T>();
			const auto v1 = stack.get<T>();

			stack.push(v1 >= v2);
			break;
		}

		case JMP: {
			i = inst.get_arg<uint32_t>(0);
			break;
		}

		case IFJMP: {
			if(stack.get<uint8_t>()) i = inst.get_arg<uint32_t>(0);
			break;
		}

		case CALL: {
			thread.call(i);
			i = inst.get_arg<uint32_t>(0);

#ifdef TUNGSTEN_VM_DEBUG
			if(vm.has_flag(DEBUG_F)) {
				cout << "Function call to " << i
					<< " (depth: " << thread.get_call_depth() << ")" << '\n';
			}
#endif

			break;
		}

		case RET: {
			i = thread.ret();

#ifdef TUNGSTEN_VM_DEBUG
			if(vm.has_flag(DEBUG_F)) {
				cout << "Function return to " << i
					<< " (depth: " << thread.get_call_depth() << ")" << '\n';
			}
#endif

			break;
		}

		case VCREATE: {
			thread.var_create<T>();
			break;
		}

		case VSTORE: {
			thread.var_store(inst.get_arg<uint32_t>(0), stack.get<T>());
			break;
		}

		case VLOAD: {
			stack.push(thread.var_load<T>(inst.get_arg<uint32_t>(0)));
			break;
		}

		case VPTR: {
			stack.push(thread.var_ptr(inst.get_arg<uint32_t>(0)));
			break;
		}

		case MSTORE: {
			const auto val = stack.get<T>();
			const auto ptr = (T*) stack.get<intptr_t>();

			*ptr = val;
			break;
		}

		case MLOAD: {
			stack.push(*((T*) stack.get<intptr_t>()));
			break;
		}

		case NCALL: {
			vm.call_native(inst.get_arg<uint32_t>(0), thread);
			break;
		}

		case EXIT: {
			vm.kill();
			break;
		}
	}
}

void VM::run(const uint32_t i)
{
	Thread thread(pthread_self());
	run(i, thread);
}

void VM::run(uint32_t i, Thread& thread)
{
	const auto& instructions = program->instructions;
	const auto size = instructions.size();

	while(i < size && !thread.is_destroyed() && !killed) {
		const auto& inst = instructions[i];

#ifdef TUNGSTEN_VM_DEBUG
		if(has_flag(INSTRUCTIONS_F)) {
			cout << i << ": " << inst.dump() << '\n';
		}
#endif

		++i;

		switch(inst.size) {
			case 1: {
				exec_instruction<int8_t>(*this, thread, i, inst);
				break;
			}

			case 2: {
				exec_instruction<int16_t>(*this, thread, i, inst);
				break;
			}

			case 4: {
				exec_instruction<int32_t>(*this, thread, i, inst);
				break;
			}

			case 8: {
				exec_instruction<int64_t>(*this, thread, i, inst);
				break;
			}

			default: {
				crash("Invalid instruction data size!");
				break;
			}
		}
	}

	if(crashed) print_stack(thread);
}

void VM::crash(const string message)
{
	if(crashed) return;
	lock_guard<mutex> lock(crash_mutex);

	crashed = true;
	kill();

	cerr << "\033[31mTungsten Virtual Machine crashed!\033[0m" << '\n';
	cerr << "---------------------------------" << '\n';
	cerr << '\n';
	cerr << "Something went wrong during TVM execution and the VM was forced to stop!" << '\n';
	cerr << '\n';

	if(!message.empty()) {
		cerr << "\t\033[1mReason: " << message << "\033[0m" << '\n';
	} else {
		cerr << "\t\033[1mReason: Unknown\033[0m" << '\n';
	}

	cerr << '\n' << '\n';
}

void VM::print_stack(const Thread& thread)
{
	lock_guard<mutex> lock(crash_mutex);

	cerr << "Thread #" << thread.get_id() << "'s stack:" << '\n';
	cerr << thread.get_stack().dump() << '\n';
	cerr << '\n';
}
