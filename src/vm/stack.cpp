#include "../tungsten.hpp"

using namespace Tungsten;

string Stack::dump() const
{
	const auto stack_size = size();

	ostringstream oss;
	oss << "Size: " << stack_size << " byte(s)" << '\n';
	oss << '\n';

	for(size_t i = 0; i < stack_size; ++i) {
		const auto& c = stack[i];
		oss << hex << setw(2) << setfill('0') << +c << dec;

		if(i < stack_size - 1) {
			oss << ',';
		}
	}

	oss << '\n' << '\n';

	return oss.str();
}
